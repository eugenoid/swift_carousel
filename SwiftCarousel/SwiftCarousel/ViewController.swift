//
//  ViewController.swift
//  SwiftCarousel
//
//  Created by Eugene Migun on 2.2.2017.
//  Copyright © 2017 BP Mobile. All rights reserved.
//

import UIKit
import iCarousel

class ViewController: UIViewController, iCarouselDelegate, iCarouselDataSource
{
	@IBOutlet weak var carouselView: iCarousel!
	
	var images = [UIImage]()
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		for _ in 1...10
		{
			self.images.append(UIImage(named: "image1")!)
		}
		
		self.carouselView.type = .custom
		self.carouselView.isPagingEnabled = true
		self.carouselView.bounces = false
		self.carouselView.reloadData()
	}

	public func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
	{
		let itemWidth = carousel.bounds.width * 2.0/3.0
		let itemHeight = carousel.bounds.height - 10
		let tempView = UIView(frame: CGRect(x: 0, y: 0, width: itemWidth, height: itemHeight))
		let frame = CGRect(x: 0, y: 0, width: itemWidth, height: itemHeight)
		let imageView = UIImageView()
		
		imageView.frame = frame
		imageView.contentMode = .scaleToFill
		imageView.image = self.images[index]
		tempView.addSubview(imageView)
		tempView.backgroundColor = UIColor.white
		return tempView
	}
	
	public func numberOfItems(in carousel: iCarousel) -> Int
	{
		return images.count
	}
	
	func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat
	{
		switch option {
		case .visibleItems:
			return 3
		case .spacing:
			return value * 1.1
		default:
			return value
		}
	}
	
	func carousel(_ carousel: iCarousel, itemTransformForOffset offset: CGFloat, baseTransform transform: CATransform3D) -> CATransform3D
	{
		print("itemTransformForOffset: \(offset)")
		
		let offsetFactor = self.carousel(carousel, valueFor: .spacing, withDefault: 1.0) * carousel.itemWidth
		let distance: CGFloat = 30.0
		let z: CGFloat = -CGFloat(fminf(1.0, fabs(Float(offset)))) * distance
		return CATransform3DTranslate(transform, offset * offsetFactor, 0.0, z)
	}
}

